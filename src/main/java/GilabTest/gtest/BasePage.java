package GilabTest.gtest;

import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import io.github.bonigarcia.wdm.WebDriverManager;

public class BasePage 
{
	public 	WebDriver driver;
	
	public WebDriver browsersetup() throws MalformedURLException
	{

	//String path =System.getProperty("user.dir");
	
		WebDriverManager.chromedriver().setup();
		//System.setProperty("webdriver.chrome.driver", "src//main//resources//driver//chromedriver");
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--headless");
//		 driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), options);
		
		driver=new  ChromeDriver(options);	
		
	driver.manage().window().maximize();
	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	return driver;
	
	}

	
	public void TearDown()
	{
	driver.quit();
	}
	
}
