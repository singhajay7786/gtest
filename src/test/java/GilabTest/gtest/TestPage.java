package GilabTest.gtest;

import java.net.MalformedURLException;

import org.openqa.selenium.WebDriver;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TestPage extends BasePage
{
	public WebDriver driver;
	
	
	@BeforeClass 
	public void initBrowser() throws MalformedURLException
	{
		driver=browsersetup();
	}
	
	
   @Test
   public void googletest()
   {
	   System.out.println("Browser started");
	   driver.get("https://www.google.com/");
	   System.out.println("Navigated to google ");
   }
   
   
   @Test
   public void googletest1()
   {
	  
	   driver.get("https://www.yahoo.com/");
	   System.out.println("Navigated to yahoo ");
   }
   
	
	@AfterClass()
	public void closeBrowser()
	{
		driver.quit();
		System.out.println("Browser closed");
	}
	
}
